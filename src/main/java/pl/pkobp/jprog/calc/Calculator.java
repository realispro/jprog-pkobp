package pl.pkobp.jprog.calc;

import pl.pkobp.jprog.calc.memory.ArrayMemory;
import pl.pkobp.jprog.calc.memory.SimpleMemory;

// SOLID
// Single Responsibility Principle
public class Calculator /*extends Object*/{

    //fields
    private Memory memory = new ArrayMemory(5, 3);

    // constructors
    public Calculator(double result) {
        System.out.println("installion memory: " + memory.toString());
        this.memory.setValue(result);
    }

    // methods non-static
    public double add(double operand){
        this.memory.setValue(this.memory.getValue()+operand);
        return this.memory.getValue();
    }

    public double subtract(double operand){
        this.memory.setValue(this.memory.getValue()-operand);
        return this.memory.getValue();
    }

    public double multiply(double operand){
        this.memory.setValue(this.memory.getValue()*operand);
        return this.memory.getValue();
    }

    public double divide(double operand) throws CalculatorException {
        this.memory.setValue(this.memory.getValue()/operand);
        return this.memory.getValue();
    }

    public double getResult() {
        return this.memory.getValue();
    }

    protected void setResult(double result) {
        this.memory.setValue(result);
    }

    // static methods

    public static int multiply(int x, int y){
        return x*y;
    }

    public static int add(int x, int y){
        return x+y;
    }

    public static int subtract(int x, int y){
        return x-y;
    }

    public static int divide(int x, int y){
        return x/y;
    }

}
