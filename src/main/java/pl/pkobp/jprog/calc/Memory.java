package pl.pkobp.jprog.calc;

public interface Memory {

    double getValue();

    void setValue(double value);

    int getSize();

}
