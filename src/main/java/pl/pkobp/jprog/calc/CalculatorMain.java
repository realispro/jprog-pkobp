package pl.pkobp.jprog.calc;

import pl.pkobp.jprog.calc.Calculator;
import pl.pkobp.jprog.calc.ScientificCalculator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalculatorMain {

    public static void main(String[] args) throws Exception {
        System.out.println("CalculatorMain.main");

        int result = Calculator.multiply(2,4);
        result = Calculator.add(result, 3);
        System.out.println("result = " + result);

        Calculator c1 = new ScientificCalculator();
        Calculator c2 = provideCalculator(); // upcasting

        c2.add(2);
        try {
            c2.divide(7);
        } catch (CalculatorException e){
            //e.printStackTrace();
            //System.exit(111);
            throw new Exception("division exception", e);
        } finally {
            System.out.println("in finally block");
        }

        c2.subtract(5.5);
        c2.multiply(1.1);

        c2.add(13.5);
        c2.multiply(0.9);

        if(c2 instanceof ScientificCalculator) {
            ScientificCalculator sc = (ScientificCalculator) c2;
            sc.power(3);
        } else {
            // TODO decide what to do otherwise
        }

        String c2String = c2.toString();
        System.out.println("c2String = " + c2String);
        
        //c1.add(2);
        c1.multiply(4);
        c1.subtract(1);
        //c1.divide(1.5);
        //c1.result = 3.14;

        Date date = new Date(); // EPOCH 1/1/1970 00:00 GMT
        long millis = date.getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //calendar.set(2020, 6, 29, 13, 54);
        calendar.add(Calendar.DATE, -1);
        date = calendar.getTime();

        Locale locale = new Locale("pl", "CA");
        DateFormat format = new SimpleDateFormat("dd%MM%YYY G", locale);
                //DateFormat.getDateInstance(DateFormat.FULL, locale);

        LocalDate ld = LocalDate.of(2020, Month.JULY,29);
        LocalTime lt = LocalTime.now();
        LocalDateTime ldt = LocalDateTime.now();
        LocalDateTime ldt2 = LocalDateTime.of(2022, 11, 1, 20,37);

        Duration duration = Duration.between(ldt, ldt2);
        System.out.println("duration = " + duration.toHours());
        
        
        ZonedDateTime warsaw = ldt.atZone(ZoneId.of("Europe/Warsaw"));
        ZonedDateTime singapore = warsaw
                .withZoneSameInstant(ZoneId.of("Asia/Singapore"))
                .plusHours(7);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd%MM%2020 HH-mm-ss-SS z");
        String timestamp = singapore.format(formatter);

        System.out.println("[" + timestamp +"] c1.result = " + c1.getResult());
        

        System.out.println("done.");

    }


    public static Calculator provideCalculator(){
        return new ScientificCalculator();
    }


}
