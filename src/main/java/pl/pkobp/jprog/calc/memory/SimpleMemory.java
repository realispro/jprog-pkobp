package pl.pkobp.jprog.calc.memory;

import pl.pkobp.jprog.calc.Memory;

public class SimpleMemory implements Memory {

    private double value;

    @Override
    public double getValue() {
        return this.value;
    }

    @Override
    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public int getSize() {
        return 1;
    }
}
