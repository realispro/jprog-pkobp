package pl.pkobp.jprog.calc.memory;

import pl.pkobp.jprog.calc.Memory;

/**
 * Memory implementation basing on array.
 * Array size is fixed to 5.
 */
public class ArrayMemory implements Memory {

    private int increase = 3;

    private double[] array; // 0, 0, 0, 0, 0

    private int index = -1;

    public ArrayMemory(int size, int increase){
        assert(size>=0);
        array = new double[size];
        this.increase = increase;
    }

    public ArrayMemory(){
        this(5,3);
    }


    @Override
    public double getValue() {
        return index>=0 ? array[index] : Double.NaN;
    }

    /**
     * Storing value in a memory.
     * Historical values are being shift in case of limit exceeded.
     * @param value value supossed to be stored in memory
     */
    @Override
    public void setValue(double value) {
        // index = 4
        // [0,1,2,3,4] + 5
        // ver1: [1,2,3,4,5] index=4
        // ver2: [5,1,2,3,4] index=0
        // ! ver3: [0,1,2,3,4,5] index=5
        if(index==array.length-1){
            // ver1: System.arraycopy( array, 1, array, 0, array.length-1);
            // ver3:
            double[] tmp = new double[array.length+increase];
            System.arraycopy(array, 0, tmp, 0, array.length);
            array = tmp;
        }
        index++;
        array[index] = value;
    }

    @Override
    public int getSize() {
        return index+1;
    }
}
