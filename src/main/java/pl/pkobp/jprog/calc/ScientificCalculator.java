package pl.pkobp.jprog.calc;

public class ScientificCalculator extends Calculator {

    public ScientificCalculator(){
        super(0.0);
        System.out.println("constructor of scientific calculator");
    }


    public double power(double operand){
        this.setResult(Math.pow(this.getResult(), operand));
        return this.getResult();
    }

    @Override
    public double divide(double operand) throws CalculatorException {
        if(operand==0){
            throw new CalculatorException("pamietaj cholero nie dziel przez zero");
        } else {
            return super.divide(operand);
        }
    }

    @Override
    public String toString() {
        return "ScientificCalculator{" +
                "result=" + getResult() +
                '}'; // char
    }
}
