package pl.pkobp.jprog.zoo;

public class Eagle extends Animal {

    public Eagle(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("eagle is flying");
    }
}
