package pl.pkobp.jprog.zoo;

public class Bear extends Animal {

    public Bear(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("bear is running!");
    }
}
