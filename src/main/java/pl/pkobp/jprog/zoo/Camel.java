package pl.pkobp.jprog.zoo;

import pl.pkobp.jprog.travel.Transportation;

public class Camel extends Animal implements Transportation {

    public Camel(String name, int size) {
        super(name, size);
    }

    @Override
    public void move() {
        System.out.println("camel is walking slowly but persistently");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is being carried by camel");
        move();
    }

    @Override
    public String getDescription() {
        return "animal vehicle";
    }
}
