package pl.pkobp.jprog.zoo;

public abstract class Animal {

    private String name;

    private int size;

    public Animal(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public void eat(String food){
        System.out.println("animal is consuming " + food);
    }

    public abstract void move();

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", size=" + size +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        if (size != animal.size) return false;
        return name != null ? name.equals(animal.name) : animal.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + size;
        return result;
    }



}
