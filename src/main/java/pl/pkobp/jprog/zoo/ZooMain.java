package pl.pkobp.jprog.zoo;

import pl.pkobp.jprog.calc.ScientificCalculator;

import java.util.*;

public class ZooMain {

    public static void main(String[] args) {
        System.out.println("ZooMain.main");

        Animal a = new Camel("Wojtek", 400);

        a.eat("fish");
        a.move();

        List<Animal> animals = new LinkedList<>();

        animals.add(a);
        animals.add(new Bear("Koralgol", 50));
        animals.add(new Eagle("Bielik", 10));
        animals.add(a);

        List<Animal> animals1 = new LinkedList<>( new HashSet<>(animals) );
        animals = animals1;

        Animal animalFromList = animals.get(animals.size()-1);
        System.out.println("animalFromList = " + animalFromList);


       /* Iterator<Animal> itr = animals.iterator();
        while(itr.hasNext()){
            Animal animal = itr.next();
            if(animal.getSize()>30){
                itr.remove();
            }
        }

        List<Integer> indexes = new ArrayList<>();
        for(int i=animals.size()-1; i>=0; i--){
            Animal animal = animals.get(i);
            if(animal.getSize()>30){
                indexes.add(i);
            }
        }
        for(int i : indexes){
            animals.remove(i);
        }*/
        
        for(Animal animal : animals){
            System.out.println("animal = " + animal);
        }

        Set<Animal> animalSet = new HashSet<>();

        System.out.println("SET:");
        animalSet.add(a);
        animalSet.add(new Bear("Koralgol", 50));
        animalSet.add(new Eagle("Bielik", 10));
        animalSet.add(a);

        animalSet.remove(a);

        for(Animal animal : animalSet){
            System.out.println("animal = " + animal);
        }
        
        Map<Animal, String> assign = new HashMap<>();
        assign.put(a, "Zenek");
        assign.put(new Eagle("Bielik", 30), "Ola");
        assign.put(new Eagle("Johny", 20), "Krzysztof");
        assign.put(new Bear("Puchatek", 330), "Jacek");
        assign.put(new Eagle("Bielik", 30), "Jola");


        Eagle bielik = new Eagle("Bielik", 30);

        String careTaker = assign.get(bielik);
        System.out.println("careTaker = " + careTaker);

        assign.remove(a);

        for( Animal key : assign.keySet()){
            System.out.println("key: " + key + ", value: " + assign.get(key));
        }
        

        System.out.println("done.");
    }

}
