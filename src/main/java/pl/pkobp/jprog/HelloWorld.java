// lowercase
package pl.pkobp.jprog;

//import static pl.pkobp.jprog.calc.Calculator.multiply;
import pl.pkobp.jprog.calc.Calculator;

import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// UpperCamelCase, PascalCase
public class HelloWorld {

    // psvm
    public static void main(String[] args) {
        System.out.println("Hey ho!"); // sout

        // primitive types
        long age = 72365764357L;  // byte, short, int, long
        System.out.println("age = " + age); // soutv

        float distance = 123.3F; // float, double
        System.out.println("distance = " + distance);

        boolean weekend = false;
        System.out.println("weekend = " + weekend);

        char c = 'a';
        System.out.println("c = " + (short) c);

        // arrays
        String[] names = /*new String[]*/{
                "Krzysiek", "Aga", "Zenek", "Ola", "Jacek"
        };

        System.out.println("names = " + Arrays.toString(names) + ", count: " + names.length);
        System.out.println("name no 3: " + names[2]);
        System.out.println("last name: " + names[names.length - 1]);

        for(int i=0;i<names.length;i++){
            System.out.println("names[" + i + "]: " + names[i]);
        }

        int i = 0;
        for(String name : names){
            System.out.println("name[" + i + "]: " + name);
            i++;
        }

        // reading program argument
        int x = args.length > 0 ? Integer.parseInt(args[0]) : weekend ? 7 : 5;
        System.out.println("x = " + x);

        Integer integer = new Integer(1);
        int i1 = integer.intValue();


        // enums
        WeekDay weekDay = WeekDay.MONDAY;

        switch(weekDay){
            case MONDAY: case TUESDAY:
                System.out.println(":(");
                break;
            case WEDNESDAY: case THURSDAY:
                System.out.println(":|");
                break;
            default:
                System.out.println(":)");
                break;
        }

        if(weekDay.ordinal()>=4){
            System.out.println(":)");
        } else if(weekDay.ordinal()>=2){
            System.out.println(":|");
        } else {
            System.out.println(":(");
        }

        // multiplication table
        int size = 15;
        int[][] result = new int[size][size];

        boolean broken = false;
        for(int iks=0; iks<size && !broken; iks++){
            for(int igrek=0;igrek<size;igrek++){
                if(igrek==12){
                    broken = true;
                    break;
                }
                result[iks][igrek] = Calculator.multiply(iks+1, igrek+1);
            }
        }

        for(int[] row : result){
            for( int value : row){
                System.out.print(value + "\t");
            }
            System.out.println();
        }

        // immutable
        String name = "Krzysztof";
        String name2 = "Krzysztof";
        //name = name.toUpperCase();

        System.out.println("name = " + name);
        System.out.println("name==name2 ? " + (name.equals(name2)));

        String join = join(new String[]{"A", "B", "C", "D"});
        System.out.println("join = " + join);

        String[] split = split("1\t2\t3\t4\t5\t6\t7\t");
        System.out.println("split = " + Arrays.toString(split));

        // \d\d-\d\d\d
        Pattern pattern = Pattern.compile("\\d\\d-\\d\\d\\d");
        Matcher matcher = pattern.matcher("Krakow 30-512, Warszawa 00-950 Poland");
        while(matcher.find()){
            String found = matcher.group();
            System.out.println("found = " + found + ", start:" + matcher.start());
        }
        
        System.out.println("done.");

    }

    public static String[] split(String s){

        StringTokenizer tokenizer = new StringTokenizer(s, "\t");
        String[] arrays = new String[tokenizer.countTokens()];

        int index = 0;
        while(tokenizer.hasMoreElements()){
            arrays[index] = tokenizer.nextToken();
            index++;
        }

        return arrays;
    }

    
    public static String join(String[] strings){
        StringBuilder sb = new StringBuilder();
        for(String s : strings){
            sb.append(s);
        }
        return sb.toString();
    }






}
