package pl.pkobp.jprog.exercise;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class Task {

    private int id;

    private Project project;

    private String topic;

    private TaskStatus status = TaskStatus.OPEN;

    private LocalDateTime created;

    private LocalDateTime closed;

    private Employee assignee;

    public Task(int id, Project project, String topic, LocalDateTime created) {
        this.id = id;
        this.project = project;
        this.project.addTask(this);
        this.topic = topic;
        this.created = created;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getClosed() {
        return closed;
    }

    public void setClosed(LocalDateTime closed) {
        this.closed = closed;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Employee getAssignee() {
        return assignee;
    }

    public void setAssignee(Employee assignee) {
        this.assignee = assignee;
        assignee.addTask(this);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", project=" + project.getName() +
                ", topic='" + topic + '\'' +
                ", status=" + status +
                ", created=" + created +
                ", closed=" + closed +
                //", assignee=" + assignee.getId() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(project, task.project) &&
                Objects.equals(topic, task.topic) &&
                status == task.status &&
                Objects.equals(created, task.created) &&
                Objects.equals(closed, task.closed) &&
                Objects.equals(assignee, task.assignee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, project, topic, status, created, closed, assignee);
    }
}
