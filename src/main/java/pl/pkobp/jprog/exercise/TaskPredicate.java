package pl.pkobp.jprog.exercise;

import java.util.function.Predicate;

public class TaskPredicate implements Predicate<Task> {

    private Employee e;

    public TaskPredicate(Employee e) {
        this.e = e;
    }

    @Override
    public boolean test(Task task) {
        return task.getAssignee()==e;
    }
}
