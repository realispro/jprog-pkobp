package pl.pkobp.jprog.exercise;

import java.util.HashSet;
import java.util.Set;

public class Project {

    private String name;

    private Set<Employee> collaborants;

    private Set<Task> tasks;

    public Project(String name) {
        this.name = name;
        this.collaborants = new HashSet<>();
        this.tasks = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Employee> getCollaborants() {
        return collaborants;
    }

    public void setCollaborants(Set<Employee> collaborants) {
        this.collaborants = collaborants;
    }

    public void addCollaborant(Employee employee){
        this.collaborants.add(employee);
        employee.addProject(this);

    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task){
        /*
        lazy object initialization
        if(tasks==null){
            tasks = new HashSet<>();
        }*/
        this.tasks.add(task);
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", collaborants=" + collaborants +
                '}';
    }
}
