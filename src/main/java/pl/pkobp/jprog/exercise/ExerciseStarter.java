package pl.pkobp.jprog.exercise;

import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExerciseStarter {

    public static void main(String[] args) {
        System.out.println("ExerciseStarter.main");

        Employee e1 = new Employee("Jacek", "Placek", 1);
        Employee e2 = new Employee("Krzysztof", "Kajak", 2);
        Employee e3 = new Employee("Piast", "Kolodziej", 3);

        Project p1 = new Project("Critical Project");
        Project p2 = new Project("Internal Tool");

        p1.addCollaborant(e1);
        p1.addCollaborant(e2);
        p1.addCollaborant(e3);

        p2.addCollaborant(e2);

        LocalDateTime now = LocalDateTime.now();
        Task t1 = new Feature(1, p1, "GUI", now);
        Task t11 = new Feature(1, p1, "GUI", now);

        Task t2 = new Feature(2, p1, "Business layer", LocalDateTime.now());
        Task t3 = new Feature(3, p1, "Dao", LocalDateTime.now());

        Task t4 = new Bug(4, p2, "Nasty bug", LocalDateTime.now());

        t2.setStatus(TaskStatus.PENDING);
        t4.setStatus(TaskStatus.CLOSED);
        t3.setStatus(TaskStatus.CLOSED);

        //t1.setAssignee(e1);
        t2.setAssignee(e2);
        t3.setAssignee(e3);

        t4.setAssignee(e2);

        // TODO start working on a task
        // TODO mark task as resolved

        List<Employee> employees = new ArrayList<>(Arrays.asList(e1, e2, e3));
        List<Project> projects = Arrays.asList(p1, p2);
        List<Task> tasks = new ArrayList<>(Arrays.asList(t1, t11, t2, t3, t4));

        //long count =
        //List<Task> processedTasks = 
        //List<String> topics =
        //boolean match =
        Optional<String> optionalString =
                tasks.stream()
                .distinct()
                //.filter( t -> t.getTopic().contains("x"))
                .filter( t -> t.getStatus()!=TaskStatus.OPEN)
                .map( t -> t.getTopic())
                .sorted( (o1,o2) -> o2.compareTo(o1))
                .findFirst();        
                //.noneMatch( s -> s.contains("x") );
                //.collect(Collectors.toList());
                //.count();

        //System.out.println("tasks count: " + count);
        //System.out.println("processedTasks = " + processedTasks);
        //System.out.println("topics = " + topics);
        //System.out.println("match = " + match);

        Supplier<RuntimeException> supplier = () -> new IllegalArgumentException("no element found");
        System.out.println("optionalString.get() = " + optionalString.orElseThrow( supplier ));

        Optional<Task> optionalTask = Optional.empty();

        if(optionalTask.isPresent()){
            Task t = optionalTask.get();
        }



/*
        Comparator<Employee> comparator = //new EmployeeComparator();
                (o1,o2) -> o2.getProjects().size()-o1.getProjects().size();*/
        employees.sort((o1,o2) -> o2.getProjects().size()-o1.getProjects().size()); // TimSort
        System.out.println("employees after sorting:" + employees);


        // filtering
        List<Employee> employeesInP2 = new ArrayList<>();
        for (Employee e : employees){
            if(e.getProjects().contains(p2)){
                employeesInP2.add(e);
            }
        }
        System.out.println("employees in p2: " + employeesInP2.toString());

        // mapping
        List<String> projectNames = new ArrayList<>();
        for(Project p : e2.getProjects()){
            projectNames.add(p.getName());
        }
        System.out.println("project names:" + projectNames.toString());


        // filtering v2
       /* Iterator<Task> itr = tasks.iterator();
        while(itr.hasNext()){
            Task t = itr.next();
            if(t.getAssignee()==e1){
                itr.remove();
                *//*e1.getTasks().remove(t);
                t.getProject().getTasks().remove(t);*//*
            }
        }*/

        Predicate<Task> taskPredicate = //new TaskPredicate(e1);
                /*new Predicate<Task>() {
                    @Override
                    public boolean test(Task task) {
                        return task.getAssignee()==e1;
                    }
                };*/
                /*(Task task) -> {
                    return task.getAssignee()==e1;
                };*/
                /*(task) -> task.getAssignee()==e1;*/
                t -> (t.getAssignee()==e1||t.getAssignee()==e2);

        tasks.removeIf(t -> t.getAssignee()==e1);

        System.out.println("tasks: " + tasks);




    }
}
