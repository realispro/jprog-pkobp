package pl.pkobp.jprog.exercise.part1;

public class Primes {

    public static void main(String[] args) {

        int maxCheck = 100; // maxCheck limit till which you want to find prime numbers

        //Empty String
        StringBuilder primeNumbersFound = new StringBuilder();

        //Start loop 1 to maxCheck
        for (int i = 1; i <= maxCheck; i++) {
            if (checkPrime(i)) {
                primeNumbersFound.append(i + " ");
            }
        }
        System.out.println("Prime numbers from 1 to " + maxCheck + " are:");
        // Print prime numbers from 1 to maxCheck
        System.out.println(primeNumbersFound.toString());
    }


    public static boolean checkPrime(int numberToCheck) {
        for (int i = 2; i <= numberToCheck / 2; i++) {
            if (numberToCheck % i == 0) {
                return false;
            }
        }
        return true;
    }

}
