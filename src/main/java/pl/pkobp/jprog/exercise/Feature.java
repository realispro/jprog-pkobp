package pl.pkobp.jprog.exercise;

import java.time.LocalDateTime;

public class Feature extends Task{

    public Feature(int id, Project project, String topic, LocalDateTime created) {
        super(id, project, topic, created);
    }
}
