package pl.pkobp.jprog.exercise;

import java.util.HashSet;
import java.util.Set;

public class Employee extends Person {

    private int id;

    private Set<Project> projects;

    private Set<Task> tasks;

    public Employee(String firstName, String lastName, int id) {
        super(firstName, lastName);
        this.id = id;
        projects = new HashSet<>();
        tasks = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addTask(Task task){
        tasks.add(task);
    }

    public void addProject(Project project){
        projects.add(project);
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public Set<Task> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                "} " + super.toString();
    }
}
