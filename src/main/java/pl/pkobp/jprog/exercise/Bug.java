package pl.pkobp.jprog.exercise;

import java.time.LocalDateTime;

public class Bug extends Task {

    public Bug(int id, Project project, String topic, LocalDateTime created) {
        super(id, project, topic, created);
    }
}
