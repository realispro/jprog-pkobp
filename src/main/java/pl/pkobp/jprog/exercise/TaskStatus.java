package pl.pkobp.jprog.exercise;

public enum TaskStatus {

    OPEN,
    PENDING,
    CLOSED

}
