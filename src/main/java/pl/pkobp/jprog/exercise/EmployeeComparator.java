package pl.pkobp.jprog.exercise;

import java.util.Comparator;

public class EmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee e1, Employee e2) {
        return e2.getProjects().size()-e1.getProjects().size();
    }
}
