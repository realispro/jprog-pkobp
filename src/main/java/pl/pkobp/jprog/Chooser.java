package pl.pkobp.jprog;

import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class Chooser {

    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>();
        // try-with-resources
        try( BufferedReader br = new BufferedReader(new FileReader(getFile())); ) {
            String line = null;
            while ((line = br.readLine()) != null) {
                numbers.add(Integer.parseInt(line));
            }
        } catch (IOException e){
            e.printStackTrace();
        }

        System.out.println("Let's choose one.");
        int choosen = 0;
        do {
            choosen = new Random(new Date().getTime()).nextInt(12) + 1;
            System.out.println("Lucky number: " + choosen);
        } while (numbers.contains(choosen));

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(getFile(), true))){
            bw.write(""+choosen);
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public static File getFile() throws IOException {

        File dir = new File("chooser");
        if(!dir.exists()){
            dir.mkdirs();
            System.out.println("Directory " + dir.getAbsolutePath() + " created.");
        } else {
            System.out.println("Directory " + dir.getAbsolutePath() + " already exists");
            for(File f : dir.listFiles( (d, n) -> n.endsWith(".txt") )){
                System.out.println("file found: " + f.getAbsolutePath());
            }
        }

        File file = new File(dir, "numbers.txt");
        if(!file.exists()){
            file.createNewFile();
            System.out.println("File " + file.getAbsolutePath() + " created");
        } else {
            System.out.println("File " + file.getAbsolutePath() + " already exists");
        }

        return file;
    }

}
