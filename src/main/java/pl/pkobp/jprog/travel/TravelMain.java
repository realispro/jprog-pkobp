package pl.pkobp.jprog.travel;

import pl.pkobp.jprog.zoo.Animal;
import pl.pkobp.jprog.zoo.Camel;

public class TravelMain {

    // psvm
    public static void main(String[] args) {
        System.out.println("TravelMain.main"); // sout

        String tourist = "Jan Kowalski";

        Transportation t = //new Camel("Johny", 300);
               p -> System.out.println("ad hoc transportation of " + p);

        startTravel(tourist, t);

        System.out.println("done.");

        Transportation.doNothing();
    }

    private static void startTravel(String tourist, Transportation t){
        t.getDescription();
        t.transport(tourist);
        // TODO accomodation
        // TODO food provider
    }

}
