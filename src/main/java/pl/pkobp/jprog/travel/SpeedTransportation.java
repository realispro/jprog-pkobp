package pl.pkobp.jprog.travel;

public interface SpeedTransportation extends Transportation{

    int getSpeedInfo();
}
