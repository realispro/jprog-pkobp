package pl.pkobp.jprog.travel;

@FunctionalInterface
public interface Transportation {

    void transport(String passenger);

    default String getDescription(){
        return getPrefix() + " no description";
    }

    static void doNothing(){
        System.out.println("doing nothing special");
    }

    private String getPrefix(){
        return "TRANSPORTATION:";
    }

}
