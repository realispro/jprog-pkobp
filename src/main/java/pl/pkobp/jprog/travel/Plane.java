package pl.pkobp.jprog.travel;

import java.io.Serializable;

public class Plane implements SpeedTransportation, Serializable {
    @Override
    public void transport(String passenger) {
        System.out.println("passenger " + passenger + " is flying");
    }

    @Override
    public String getDescription() {
        return "transporting on an air";
    }

    @Override
    public int getSpeedInfo() {
        return 1000;
    }
}
